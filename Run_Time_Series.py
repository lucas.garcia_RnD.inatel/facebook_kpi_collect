from csv import reader
import os
import platform

from Connections import con_bacos
import mysql.connector as mysql
import pandas as pd
from datetime import datetime

def time_series_concat(download_path):

    try:
        # concatenar as 3 tabelas do output do facebook no modelo do BACO facebook.Time_Series
        uf_dict = {'Rondônia':'RO',
                   'Acre':'AC',
                   'Amazonas':'AM',
                   'Roraima':'RR',
                   'Pará':'PA',
                   'Amapá':'AP',
                   'Tocantins':'TO',
                   'Maranhão':'MA',
                   'Piauí':'PI',
                   'Ceará':'CE',
                   'Rio Grande do Norte':'RN',
                   'Paraíba':'PB',
                   'Pernambuco':'PE',
                   'Alagoas':'AL',
                   'Sergipe':'SE',
                   'Bahia':'BA',
                   'Minas Gerais':'MG',
                   'Espírito Santo':'ES',
                   'Rio de Janeiro':'RJ',
                   'São Paulo':'SP',
                   'Paraná':'PR',
                   'Santa Catarina':'SC',
                   'Rio Grande do Sul':'RS',
                   'Mato Grosso do Sul':'MS',
                   'Mato Grosso':'MT',
                   'Goiás':'GO',
                   'Distrito Federal':'DF',
                   }
        tec_dict = {'Download Speed':'Download_Speed',
                    'Downstream Latency':'Downstream_Latency',
                    'Round-Trip Time':'Round_Trip_Time'}

        #Lendo dataframes
        all_tables = [f for f in os.listdir(download_path) if os.path.isfile(os.path.join(download_path, f))]
        #definindo UF
        current_mo_dir = os.path.join(download_path,all_tables[0])
        uf_select = pd.read_csv(current_mo_dir)
        UF = uf_dict[list(uf_select.columns)[1]]
        for table in all_tables:
            current_mo_dir = os.path.join(download_path,table)
            if 'Download Speed' in table:
                columns = ['DATE','Download_Speed']
                speed_df = pd.read_csv(current_mo_dir)
                speed_df.columns = columns
                os.remove(current_mo_dir)
            if 'Downstream Latency' in table:
                columns = ['DATE','Downstream_Latency']
                latency_df = pd.read_csv(current_mo_dir)
                latency_df.columns = columns
                os.remove(current_mo_dir)
            if 'Round-Trip Time' in table:
                columns = ['DATE','Round_Trip_Time']
                trip_df = pd.read_csv(current_mo_dir)
                trip_df.columns = columns
                os.remove(current_mo_dir)

        #modelando dataframes
        final_df = pd.merge(speed_df,latency_df,how='inner',on='DATE')
        final_df = pd.merge(final_df,trip_df,how='inner',on='DATE')
        final_df.insert(1,'UF', UF, allow_duplicates=True)
        final_df['DATE'] = pd.to_datetime(final_df["DATE"])
        final_df['DATE'] = final_df["DATE"].dt.strftime("%Y-%m-%d")

        #salvando df_to_csv new name
        time_series_file = os.path.abspath(os.path.join(download_path, f'Time_Series.csv'))
        final_df.to_csv(time_series_file, index = None, header=True)

        return True
    except:
        return False



def check_csv(actual_path):
    print(f'Cheking file')
    df = pd.read_csv(actual_path)
    column_check = list(df.columns)

    if str(column_check[0]).isnumeric():
        return True
    else:
        return False


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #definindo diretorios
    basepath = os.path.dirname(__file__)
    download_path = os.path.abspath(os.path.join(basepath, "download"))

    #concatenando 3 tabelas em 1 Time_Series
    check_concat = time_series_concat(download_path)

    if check_concat:
        # conectando ao NPsmart
        all_tables = [f for f in os.listdir(download_path) if os.path.isfile(os.path.join(download_path, f))]

        # criando cursor do bd
        con_bacos = con_bacos()

        db_connect = mysql.connect(
            host="10.26.82.160",
            user='lrgsouza',
            passwd='luc4S2020',
            database=f'facebook',
            allow_local_infile=True
        )
        cursor = db_connect.cursor()

        print(f'>>>>>> START TIME_SERIES DATABASE UPLOAD <<<<<<<')
        start = datetime.now()
        for current_table in all_tables:

            table = current_table.replace('.csv', '')

            now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
            csv_done_file = os.path.abspath(os.path.join(download_path, 'done', f'{table}_{now}.csv'))

            #subindo CSV na tabela
            csv_final_path = os.path.join(download_path, current_table)
            upload_csv_path = csv_final_path.replace('\\', '/')

            cur_platform = platform.system()
            print(f'Loading {table}...')
            if cur_platform.upper() == 'WINDOWS':
                sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE facebook.{table}\n' \
                      f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                      f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
            else:
                sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE facebook.{table}\n' \
                      f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                      f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
            cursor.execute(sql)
            db_connect.commit()
            print(f'Done: {table}')
            os.rename(csv_final_path, csv_done_file)

        db_connect.close()
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Uploaded in {delta}.')
        print(f'>>>>>> FINISH TIME_SERIES DATABASE UPLOAD <<<<<<<')