########MODULOS AUTOMAÇÂO WEB#########
import os
import shutil
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import ElementClickInterceptedException
import time
import pyautogui

##################################### FUNÇÕES #############################################
from Regional_Maps_Extractor_Functions import autogui_left_click, autogui_wait, autogui_double_click, \
    autogui_right_click, autogui_move, espera_download
from Run_Regional_Maps import Run_regional_Maps_Process


def espera_clica(xpath,timeout=20):
    while True:
        try:
            wait = WebDriverWait(driver,timeout)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, xpath)))
            element.click()
            break
        except ElementClickInterceptedException:
            time.sleep(1)


def espera_clica_css(css,timeout=20):
    while True:
        try:
            wait = WebDriverWait(driver,timeout)
            element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, css)))
            element.click()
            break
        except ElementClickInterceptedException:
            time.sleep(1)

def clean_all(download_path):
    files = [f for f in os.listdir(download_path) if os.path.isfile(os.path.join(download_path, f))]
    for file in files:
        os.remove(os.path.join(download_path, file))
    folders = os.listdir(os.path.join(download_path, 'date_process'))
    for folder in folders:
        shutil.rmtree(os.path.join(download_path, 'date_process', folder))

#=======================================definindo parametros===============================
#True = roda apenas 1 dia // False = Roda quantidade de dias em "days_to_collect" 1 a 30
today_flag = True
days_to_collect = 30

#criando driver!
#driver = webdriver.Chrome()
basepath = os.path.dirname(__file__)
download_path = os.path.abspath(os.path.join(basepath, "download"))
chromeOptions = webdriver.ChromeOptions()
prefs = {"download.default_directory" : download_path}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "chromedriver.exe"
driver = webdriver.Chrome(options=chromeOptions)

#limpando pastas
clean_all(download_path)

#abrindo site facebook
driver.get("https://partners.facebook.com/actionable_insights/apps/?partner_id=45697660920&app=network_insights&section=ni_time_series")

#maximizando janela driver
driver.maximize_window()

#tela de alerta facebook para login
espera_clica('//*[@id="facebook"]/body/div/div[1]/div[2]/div/div/div/div/div[3]/div/div[1]/a[2]')

#define login/senha
login = '21 9 9189 9972'
senha = 'Matsqm123%'

#efetua login
driver.find_element(By.ID,"email").send_keys(login)
driver.find_element(By.ID,"pass").send_keys(senha)
driver.find_element(By.ID,"loginbutton").click()

#espera tela inicial
if autogui_wait('overview.png', 30):
    pass

# EXTRAÇÃO EXTRAÇÃO REGINAL MAPS
driver.get("https://partners.facebook.com/actionable_insights/apps/?partner_id=45697660920&app=network_insights&section=network_insights")
#time.sleep(5)

#define list
template_list = ['4g_Download-Speed.png','4g_Downstream-Latency.png','4g_Round-Trip-Time.png']
#template_list = ['4g_Round-Trip-Time.png']

#loop para templates
for template in template_list:
    actual_process = template.replace('.png','').split('_')[1]
    print(f'Starting: Regional_Maps_{actual_process}')
    #procura e clica nos templates salvos
    autogui_left_click('seta_template.png', 10)
    autogui_move('coleta_tx.png', 10)
    autogui_left_click(template, 10)
    autogui_left_click('4_weeks.png', 10)
    autogui_left_click('over_4_weeks.png', 10)
    autogui_left_click('over_1_day.png', 10)
    time.sleep(2)
    #botão apply data incial
    espera_clica('//*[@id="facebook"]/body/div[5]/div[1]/div[1]/div/div[3]/span/div/div')
    #espera carregar
    if autogui_wait('search_regions.png', 30):
        pass

    #acha elemento pelo nome da classe
    content = driver.find_elements(By.CSS_SELECTOR, '[class="l5k26z4s snfsxcfg jbfqbifr oysqpf8i n54jr4lg kiex77na lgsfgr3h mcogi7i5 ih1xi9zn a53abz89"]')
    #define data incial
    data_inicial = content[0].text.split('(')[0].strip()
    data = datetime.strptime(data_inicial, '%b %d, %Y').strftime('%Y-%m-%d')

    #criando primeira pasta
    act_download_path = os.path.abspath(os.path.join(download_path, "date_process",str(data)))
    try:
        os.mkdir(act_download_path)
    except:
        pass

    #download
    print(f'Downloading: {data} - Regional_Maps_{actual_process}')
    autogui_left_click('download_button.png', 10)
    ne_file = espera_download(download_path)

    #identificar download da raiz e copiar para pasta da data atual
    file_path = os.path.abspath(os.path.join(download_path,ne_file))
    new_ne_file_name = f'{data}_Regional_Maps_{actual_process}.csv'
    save_path = os.path.abspath(os.path.join(act_download_path,new_ne_file_name))
    os.rename(file_path, save_path)
    print(f'Finish: {data} - Regional_Maps{actual_process}')

    #loop para proximos dias
    if not today_flag: # define se vai coletar 1 dia ou 30
        for i in range(1,days_to_collect):
            #mudando dia
            # ===== clica no botao principal
            espera_clica_css('[class="l5k26z4s snfsxcfg jbfqbifr oysqpf8i n54jr4lg kiex77na lgsfgr3h mcogi7i5 ih1xi9zn a53abz89"]')
            # ===== clica no dia
            autogui_left_click('day_select.png', 10)
            # ===== muda pro proximo
            time.sleep(1)
            # for i in range (1,20):
            #     pyautogui.hotkey("tab")
            #     time.sleep(0.1)
            time.sleep(0.5)
            pyautogui.hotkey("tab")
            time.sleep(0.5)
            pyautogui.press("down")
            time.sleep(0.5)
            pyautogui.press("enter")
            #apply
            espera_clica('//*[@id="facebook"]/body/div[5]/div[1]/div[1]/div/div[3]/span/div/div')
            #espera carregar
            if autogui_wait('search_regions.png', 30):
                pass
            #dia mudado

            #define data incial
            content = driver.find_elements(By.CSS_SELECTOR, '[class="l5k26z4s snfsxcfg jbfqbifr oysqpf8i n54jr4lg kiex77na lgsfgr3h mcogi7i5 ih1xi9zn a53abz89"]')
            data_inicial = content[0].text.split('(')[0].strip()
            data = datetime.strptime(data_inicial, '%b %d, %Y').strftime('%Y-%m-%d')

            #criando primeira pasta
            act_download_path = os.path.abspath(os.path.join(download_path, "date_process",str(data)))
            try:
                os.mkdir(act_download_path)
            except:
                pass

            #download
            print(f'Downloading: {data} - Regional_Maps_{actual_process}')
            autogui_left_click('download_button.png', 10)
            time.sleep(3)
            ne_file = espera_download(download_path)

            #identificar download da raiz e copiar para pasta da data atual
            file_path = os.path.abspath(os.path.join(download_path,ne_file))
            new_ne_file_name = f'{data}_Regional_Maps_{actual_process}.csv'
            save_path = os.path.abspath(os.path.join(act_download_path,new_ne_file_name))
            os.rename(file_path, save_path)
            print(f'Finish: {data} - Regional_Maps_{actual_process}')

    print(f'Finish: Regional_Maps_{actual_process}')
    Run_regional_Maps_Process()

print(f'Finish Regional Maps Process')
driver.quit()


