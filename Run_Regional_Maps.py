
import os
import platform
import mysql.connector as mysql
import pandas as pd
from datetime import datetime

def df_update(df_csv,key_element,now=''):

    # concatenar as 3 tabelas do output do facebook no modelo do BACO facebook.Time_Series
    uf_dict = {'Rondônia':'RO',
               'Acre':'AC',
               'Amazonas':'AM',
               'Roraima':'RR',
               'Pará':'PA',
               'Amapá':'AP',
               'Tocantins':'TO',
               'Maranhão':'MA',
               'Piauí':'PI',
               'Ceará':'CE',
               'Rio Grande do Norte':'RN',
               'Paraíba':'PB',
               'Pernambuco':'PE',
               'Alagoas':'AL',
               'Sergipe':'SE',
               'Bahia':'BA',
               'Minas Gerais':'MG',
               'Espírito Santo':'ES',
               'Rio de Janeiro':'RJ',
               'São Paulo':'SP',
               'Paraná':'PR',
               'Santa Catarina':'SC',
               'Rio Grande do Sul':'RS',
               'Mato Grosso do Sul':'MS',
               'Mato Grosso':'MT',
               'Goiás':'GO',
               'Distrito Federal':'DF',
               }
    if now == '':
        now = datetime.now().date().strftime("%Y-%m-%d")

    df_csv[['Cidade','UF']] = df_csv['Region'].str.split(',',expand=True)
    df_csv.drop('Region', axis=1, inplace=True)
    df_csv.insert(0,'DATE', f'{now}', allow_duplicates=True)
    df_csv = df_csv[['DATE','Cidade','UF',key_element,'Samples']]
    df_csv['UF'] = df_csv['UF'].str.strip()
    for old, new in uf_dict.items():
        df_csv['UF'] = df_csv['UF'].str.replace(old, new, regex=False)
    #formatando coluna cidade
    df_csv['Cidade'] = df_csv['Cidade'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    df_csv['Cidade'] = df_csv['Cidade'].replace('  ', ' ', regex=True)
    df_csv['Cidade'] = df_csv['Cidade'].replace(' ', '_', regex=True)
    df_csv['Cidade'] = df_csv['Cidade'].replace("'", '', regex=True)
    df_csv['Cidade'] = df_csv['Cidade'].str.upper()

    return df_csv


def check_csv(actual_path):
    df = pd.read_csv(actual_path)
    column_check = list(df.columns)
    if str(column_check[0]).isnumeric():
        return True
    else:
        return False

def date_folders_process(download_path):

    date_path = os.path.abspath(os.path.join(download_path, "date_process"))
    #schema dict
    table_dict = {'Downstream Latency, ms':'Regional_Maps_Downstream_Latency',
                  'Download Speed, Mbps':'Regional_Maps_Download_Speed',
                  'Round-Trip Time, ms':'Regional_Maps_Round_Trip_Time'}

    #procurando pastas
    date_paths = os.listdir(date_path)

    #definindo conexão com baco
    db_connect = mysql.connect(
        host="10.26.82.160",
        user='lrgsouza',
        passwd='luc4S2020',
        database=f'facebook',
        allow_local_infile=True
    )
    cursor = db_connect.cursor()

    print(f'>>>>>> START REGIONAL_MAPS - Date folders - DATABASE UPLOAD <<<<<<<')
    start = datetime.now()

    for date in date_paths:

        #definindo pasta para processar
        actual_date_path = os.path.abspath(os.path.join(date_path,date))
        #extraindo hora
        now = date

        print(f'>>>>>> PROCESSING {date} <<<<<<<')
        all_tables = [f for f in os.listdir(actual_date_path) if os.path.isfile(os.path.join(actual_date_path, f))]

        for current_table in all_tables:

            # CHECK IF TABLE EXISTS
            current_mo_dir = f'{actual_date_path}\\{current_table}'
            #==========check columns
            column_jump = check_csv(current_mo_dir)

            #abrindo CSV para DF
            if column_jump:
                df_csv = pd.read_csv(current_mo_dir,skiprows=1)
            else:
                df_csv = pd.read_csv(current_mo_dir)


            list_colums = list(df_csv.columns)

            #select table by dict
            table = table_dict[list_colums[1]]

            new_columns = []
            csv_done_file = os.path.abspath(os.path.join(download_path, 'done', f'{table}_{now}.csv'))

            #tratando DF
            df_csv = df_update(df_csv,list_colums[1],now)
            list_colums = list(df_csv.columns)

            #criando tabela se o comando for novo
            sql_table_creation = f'CREATE TABLE IF NOT EXISTS facebook.{table} ('
            for column in list_colums:
                if not str(column).upper() == 'DATE':
                    column = column.replace('.','')
                    column = column.replace('/','_')
                    column = column.replace('-','_')
                    column = column.replace(',','')
                    column = column.replace(' ','_')
                    new_columns.append(column)
                    sql_insert_col = f"{column} varchar(100) DEFAULT '-', "
                    sql_table_creation = sql_table_creation + sql_insert_col
                else:
                    column = column.replace('.','')
                    column = column.replace('-','_')
                    column = column.replace(' ','_')
                    new_columns.append(column)
                    sql_insert_col = f"{column} date DEFAULT NULL, "
                    sql_table_creation = sql_table_creation + sql_insert_col

            sql_table_creation = sql_table_creation[:-2] + ');'
            cursor.execute(sql_table_creation)
            db_connect.commit()

            #deletando tabela antiga
            csv_final_path = os.path.join(actual_date_path, current_table)
            os.remove(csv_final_path)

            #salvando nova tabela
            df_csv.columns = new_columns
            df_csv.to_csv(csv_done_file, index = None, header=True)

            #subindo arquivo
            csv_done_file = csv_done_file.replace('\\', '/')
            cur_platform = platform.system()
            print(f'Loading {table}...')
            if cur_platform.upper() == 'WINDOWS':
                sql = f'LOAD DATA LOCAL INFILE "{csv_done_file}" REPLACE INTO TABLE facebook.{table}\n' \
                      f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                      f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
            else:
                sql = f'LOAD DATA LOCAL INFILE "{csv_done_file}" REPLACE INTO TABLE facebook.{table}\n' \
                      f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                      f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
            cursor.execute(sql)
            db_connect.commit()
            print(f'Done: {table}_{now}')

        os.rmdir(actual_date_path)
        print(f'>>>>>> DONE FOLDER {date} <<<<<<<')

    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'Uploaded in {delta}.')
    print(f'>>>>>> FINISH REGIONAL_MAPS - Date folders - DATABASE UPLOAD <<<<<<<')

    return True

def today_process(download_path):

    #schema dict
    table_dict = {'Downstream Latency, ms':'Regional_Maps_Downstream_Latency',
                  'Download Speed, Mbps':'Regional_Maps_Download_Speed',
                  'Round-Trip Time, ms':'Regional_Maps_Round_Trip_Time'}

    # conectando ao NPsmart
    all_tables = [f for f in os.listdir(download_path) if os.path.isfile(os.path.join(download_path, f))]

    db_connect = mysql.connect(
        host="10.26.82.160",
        user='lrgsouza',
        passwd='luc4S2020',
        database=f'facebook',
        allow_local_infile=True
    )
    cursor = db_connect.cursor()

    print(f'>>>>>> START REGIONAL_MAPS DATABASE UPLOAD <<<<<<<')
    start = datetime.now()
    for current_table in all_tables:

        # CHECK IF TABLE EXISTS
        current_mo_dir = f'{download_path}\\{current_table}'
        #==========check columns
        column_jump = check_csv(current_mo_dir)

        #abrindo CSV para DF
        if column_jump:
            lines_to_jump = 2
            df_csv = pd.read_csv(current_mo_dir,skiprows=1)
        else:
            lines_to_jump = 1
            df_csv = pd.read_csv(current_mo_dir)


        list_colums = list(df_csv.columns)

        #select table by dict
        table = table_dict[list_colums[1]]

        new_columns = []
        now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        csv_done_file = os.path.abspath(os.path.join(download_path, 'done', f'{table}_{now}.csv'))

        #tratando DF
        df_csv = df_update(df_csv,list_colums[1])
        list_colums = list(df_csv.columns)

        #criando tabela se o comando for novo
        sql_table_creation = f'CREATE TABLE IF NOT EXISTS facebook.{table} ('
        for column in list_colums:
            if not str(column).upper() == 'DATE':
                column = column.replace('.','')
                column = column.replace('/','_')
                column = column.replace('-','_')
                column = column.replace(',','')
                column = column.replace(' ','_')
                new_columns.append(column)
                sql_insert_col = f"{column} varchar(100) DEFAULT '-', "
                sql_table_creation = sql_table_creation + sql_insert_col
            else:
                column = column.replace('.','')
                column = column.replace('-','_')
                column = column.replace(' ','_')
                new_columns.append(column)
                sql_insert_col = f"{column} date DEFAULT NULL, "
                sql_table_creation = sql_table_creation + sql_insert_col

        sql_table_creation = sql_table_creation[:-2] + ');'
        cursor.execute(sql_table_creation)
        db_connect.commit()

        #deletando tabela antiga
        csv_final_path = os.path.join(download_path, current_table)
        os.remove(csv_final_path)

        #salvando nova tabela
        df_csv.columns = new_columns
        df_csv.to_csv(csv_done_file, index = None, header=True)

        #subindo arquivo
        csv_done_file = csv_done_file.replace('\\', '/')
        cur_platform = platform.system()
        print(f'Loading {table}...')
        if cur_platform.upper() == 'WINDOWS':
            sql = f'LOAD DATA LOCAL INFILE "{csv_done_file}" REPLACE INTO TABLE facebook.{table}\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
        else:
            sql = f'LOAD DATA LOCAL INFILE "{csv_done_file}" REPLACE INTO TABLE facebook.{table}\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
        cursor.execute(sql)
        db_connect.commit()
        print(f'Done: {table}')


    db_connect.close()
    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'Uploaded in {delta}.')
    print(f'>>>>>> FINISH REGIONAL_MAPS DATABASE UPLOAD <<<<<<<')

    return True

def Run_regional_Maps_Process():
    #FLAGS =================== DATE_FOLDERS | TODAY_ONLY

    #definindo diretorios
    basepath = os.path.dirname(__file__)
    download_path = os.path.abspath(os.path.join(basepath, "download"))
    date_folders_process(download_path)
    #PROCESSANDO TABELAS
    # if today_only:
    #     today_process(download_path)
    # else:
    #     date_folders_process(download_path)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    #FLAGS =================== DATE_FOLDERS | TODAY_ONLY
    date_folders = True

    #definindo diretorios
    basepath = os.path.dirname(__file__)
    download_path = os.path.abspath(os.path.join(basepath, "download"))

    #PROCESSANDO TABELAS
    if date_folders:
        date_folders_process(download_path)
    else:
        today_process(download_path)

