########MODULOS AUTOMAÇÂO WEB#########
import pyautogui
import os
import time

def autogui_wait(file, time_wait):
    time.sleep(0.5)
    timeout = 0
    while True:
        if pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9):
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1

def autogui_double_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            pyautogui.doubleClick(pos)
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_right_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            #pyautogui.click(f'prints\\{file}', button='right')
            pyautogui.click(pos, button='right')
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_left_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            pyautogui.click(pos)
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1

def autogui_move(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            pyautogui.moveTo(pos)
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1

def espera_download(download_path):
    seconds = 0
    dl_wait = True
    time.sleep(5)
    while dl_wait and seconds < 600:
        dl_wait = False
        ne_files = [f for f in os.listdir(download_path) if os.path.isfile(os.path.join(download_path, f))]
        if ne_files:
            ne_file = ne_files[0]
            if '.csv' in ne_file:
                print('Download terminado!')
                return ne_file
            else:
                dl_wait = True
                seconds += 1
                continue
        else:
            dl_wait = True
            seconds += 1
            continue
